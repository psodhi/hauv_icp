#!/bin/sh

# make dir
DATA_DIR=../data/dataset_shiphull
mkdir -p $DATA_DIR
rm -r $DATA_DIR/*
cd $DATA_DIR

# get data
wget http://www.frc.ri.cmu.edu/~psodhi/datasets/hauv_icp/dataset_shiphull.zip 
unzip dataset_shiphull

# remove zip dirs
rm dataset_shiphull.zip

# display output
echo "\nWrote Shiphull Dataset Files to : $DATA_DIR\n"