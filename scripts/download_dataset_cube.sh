#!/bin/sh

# make dir
DATA_DIR=../data/dataset_cube
mkdir -p $DATA_DIR
rm -r $DATA_DIR/*
cd $DATA_DIR

# get data
wget http://www.frc.ri.cmu.edu/~psodhi/datasets/hauv_icp/dataset_cube.zip 
unzip dataset_cube

# remove zip dirs
rm dataset_cube.zip

# display output
echo "\nWrote Cube Dataset Files to : $DATA_DIR\n"
